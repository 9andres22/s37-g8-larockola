CREATE DATABASE s37g8rockola;

use s37g8rockola;

CREATE TABLE cancion(
id int  primary key not null auto_increment,
nombre varchar (45),
autor varchar (45),
genero Varchar (45),
enlace text not null,
comentario text (100)
);

INSERT INTO cancion VALUES 
(1,'N.I.B. (2009 - Remaster)', 'Black Sabbath',5,"https://www.youtube.com/watch?v=nJYdDmUvgok&ab_channel=BlackSabbath-Topic","null"),
(2,'War Pigs / Luke´s Wall (2012 - Remaster)', 'Black Sabbath',5,"https://www.youtube.com/watch?v=zVY--knsDc4&ab_channel=BlackSabbath-Topic","null"),
(3,'Iron Man (2012 - Remaster)', 'Black Sabbath',5,"https://www.youtube.com/watch?v=b3-QqGVt-tM&ab_channel=BlackSabbath-Topic","null");