<%-- 
    Document   : eliminar
    Created on : Oct 1, 2021, 11:38:47 PM
    Author     : diego
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="ModeloDAO.cancionDAO"%>
<%@page import="java.util.List"%>
<%@page import="com.google.gson.Gson"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Eliminar-laRockola.com</title>
    <div class="contenedor">
        <h1>laRockola.com .<span>&#160;</span></h1>
    </div>
    <hr>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <style>
        .body{
            background-color: pink;
        }
        
        .col-10 {
            margin:0 auto;
            text-align: center;
            background-color: pink;
            border-radius: 25px;
            padding: 20px;
        }
        h2.th2{
            text-align: center;
        }
        .cbgen{
                text-align: center;
        }
        .botones{
            text-align: center;
        }
        h1.rockola{
            text-align: center;
            font-weight: bold;
        }
        .contenedor {
            margin: auto;
            display: table;
        }
        h1 { 
            position: relative; 
            float: left;
            background: white;
            color: #000;
            font-size: 2.5em;
        }
        h1 span {
            position:absolute;
            right:0;
            width:0;
            background: white;
            border-left: 1px solid #000;
            animation: escribir 5s steps(30) infinite alternate;
        }
        @keyframes escribir {
            from { width: 100% }
            to { width:0 }
        }
    </style>
    </head>
<body>
    <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href=Controlador?accion=home">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Eliminar</li>
            </ol>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-10">
                <h2 class ="th2">¡Selecciona la cancion a eliminar!</h2>
                <br>
                <form action="eliminar.jsp">
                    <select name="cbnombre">
                        <option value="">Selecciona una cancion a eliminar </option>
                        <%
                            List<String> delete = new ArrayList<>();
                            cancionDAO cn = new cancionDAO();
                            delete = cn.mostrarnombres();
                            for (String cat : delete) {
                        %>
                        <option value="<%=cat%>"><%=cat%>
                        </option>
                        <%
                            }
                        %>
                    </select>
                    <br>
                    <br>
                    <div id="liveAlertPlaceholder"></div>
                    <button type="submit" class="btn btn-danger">Eliminar cancion</button>
                </form>
                <br>
                <br>
                <br>
                <br>
                <hr>
                <a href="Controlador?accion=home" class="btn btn-primary">Volver a ver lista de canciones</a>
            </div>
        </div>
    </div>
    <%  if (request.getParameter("cbnombre") != null) {
            boolean elim = false;
            String nom = request.getParameter("cbnombre");
            cancionDAO CDAO = new cancionDAO();
            elim = CDAO.eliminar(nom);
            if (elim == true) {
                String mensaje = "<script language='javascript'>alert('Se ha eliminado la cancion de forma exitosa!');</script>";
                out.println(mensaje);
            }
        }
    %>
</body>

</html>