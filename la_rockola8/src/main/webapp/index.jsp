<%-- 
    Document   : index
    Created on : Sep 27, 2021, 8:46:58 PM
    Author     : diego
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.List"%>
<%@page import="Modelo.Cancion"%>
<%@page import="ModeloDAO.cancionDAO"%>
<%@page import="com.google.gson.Gson"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>   
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>laRockola.com</title>
    <div class="contenedor">
        <h1>laRockola.com .<span>&#160;</span></h1>
    </div>
    <hr>
    <link href="assets\bootstrap-5.1.1-dist\css\bootstrap.min.css" rel="stylesheet">
    <style>
        .col-10 {
            margin:0 auto;
        }  
        h2.nuestras{
            text-align: center;
        }
        .cbgen{
            text-align: center;
        }
        .botones{
            text-align: center;
        }
        h1.rockola{
            text-align: center;
            font-weight: bold;
        }
        .contenedor {
            margin: auto;
            display: table;
        }
        h1 { 
            position: relative; 
            float: left;
            background: white;
            color: #000;
            font-size: 2.5em;
        }
        h1 span {
            position:absolute;
            right:0;
            width:0;
            background: white;
            border-left: 1px solid #000;
            animation: escribir 5s steps(30) infinite alternate;
        }
        @keyframes escribir {
            from { width: 100% }
            to { width:0 }
        }
    </style>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-10">
                <h2 class="nuestras">Nuestras canciones!</h2>
                <form>
                    <div class="cbgen">
                        <select name="cbgenero">
                            <option value="">Selecciona un genero musical</option>
                            <%

                                Set<String> conj = new TreeSet<>();
                                cancionDAO cn = new cancionDAO();
                                conj = cn.mostrar();
                                for (String cat : conj) {
                            %>
                            <option value="<%=cat%>"><%=cat%>
                            </option>
                            <%
                                }
                            %>

                        </select>
                        <input type="submit" name="accion" value="Mostrar lista de canciones"/><br>
                    </div>
                </form>

                <br>      
                <table class="table table-dark table-striped">
                    <tr>

                        <th><center>Nombre</center></th>
                    <th><center>Artista</center></th>
                    <th><center>Genero</center></th>
                    <th><center>Enlace YT</center></th>
                    <th><center>Comentarios</center></th>

                    </tr>
                    <%
                        cancionDAO dao = new cancionDAO();

                        if (request.getParameter("cbgenero") == null || request.getParameter("cbgenero") == "") { //Condicional para mostrar canciones aleatorias
                            List<Integer> idlist = dao.selectid(); //Conexion con base de datos para sacar los id de la BD
                            Set<Integer> aleaconj = new TreeSet<>();
                            do {
                                int num = (int) Math.floor(Math.random() * ((idlist.size() - 1) - 0 + 1) + 0); //Para generar numeros aleatorios que esten comprendidos dentro de los ID
                                aleaconj.add(idlist.get(num));
                            } while (aleaconj.size() < 3); //Loop para almacenar 3 id de la base de datos y no esten repetidos
                            List<Integer> alea = new ArrayList();
                            alea.addAll(aleaconj);
                            for (int i = 0; i < alea.size(); i++) {

                                List<Cancion> canale = dao.listarale(alea.get(i));

                                for (Cancion can : canale) {

                                    out.println("<tr>");
                                    out.println("<td><center>" + can.getNom() + "</td><center>");
                                    out.println("<td><center>" + can.getAutor() + "</td><center>");
                                    out.println("<td><center>" + can.getGen() + "</td><center>");
                                    out.println("<td><center><a href=\"" + can.getEnlace() + "\" target=\"_blank\">Link</a><center></td>");
                                    out.println("<td><center>" + can.getComentario() + "</td><center>");
                                    out.println("</tr>");
                                }

                            }
                        } else {

                            //Camino que muestra las canciones que solicita el usuario por genero
                            String gener = request.getParameter("cbgenero");
                            List<Cancion> canciones = dao.list(gener);

                            for (Cancion can : canciones) {
                                out.println("<tr>");
                                out.println("<td><center>" + can.getNom() + "</td><center>");
                                out.println("<td><center>" + can.getAutor() + "</td><center>");
                                out.println("<td><center>" + can.getGen() + "</td><center>");
                                out.println("<td><center><a href=\"" + can.getEnlace() + "\" target=\"_blank\">Link</a><center></td>");
                                out.println("<td><center>" + can.getComentario() + "</td><center>");
                                out.println("</tr>");
                            }
                        }
                    %>
                </table>    
                <hr>
                <div class="botones">
                    <a href="Controlador?accion=agregar" class="btn btn-success">Agregar cancion</a>
                    <a href="Controlador?accion=editar" class="btn btn-secondary">Editar cancion</a>
                    <a href="Controlador?accion=eliminar" class="btn btn-danger">Eliminar cancion</a>  
                </div>
            </div>
        </div>
    </div>
    <script src="assets\bootstrap-5.1.1-dist\js\bootstrap.min.js"></script>  
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
</body>
</html>