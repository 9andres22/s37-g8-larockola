<%-- 
    Document   : editar
    Created on : Oct 2, 2021, 11:17:01 PM
    Author     : diego
--%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.Set"%>
<%@page import="Modelo.Cancion"%>
<%@page import="ModeloDAO.cancionDAO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar-laRockola.com</title>
        <div class="contenedor">
            <h1>laRockola.com .<span>&#160;</span></h1>
        </div>
        <hr>
         <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
         <style>
             .body{
                 background-color: pink;
             }

             .col-10 {
                 margin:0 auto;
                 text-align: center;
                 background-color: #b6effb;
                 border-radius: 25px;
                 padding: 20px;
             }
             h2.th2{
                 text-align: center;
             }
             .row{
                 text-align: center;
                 margin: 0 auto;
             }
            
             h1.rockola{
                 text-align: center;
                 font-weight: bold;
             }
             .contenedor {
                 margin: auto;
                 display: table;
             }
             h1 { 
                 position: relative; 
                 float: left;
                 background: white;
                 color: #000;
                 font-size: 2.5em;
             }
             h1 span {
                 position:absolute;
                 right:0;
                 width:0;
                 background: white;
                 border-left: 1px solid #000;
                 animation: escribir 5s steps(30) infinite alternate;
             }
             @keyframes escribir {
                 from { width: 100% }
                 to { width:0 }
             }

         </style>
    </head>
    <body>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href=Controlador?accion=home">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Editar</li>
            </ol>
        </nav>
        <div class="container-fluid">
            <div class="row">
                <div class="col-10">
                    <h2>¡Edita la cancion seleccionada!</h2>
                    <br>
                    <br>
                    <form id="formulario1" method="POST" action="editar.jsp">
                        <select name="cbnombre">
                            <option value=" ">Selecciona una cancion a editar </option>
                            <%
                                List<String> edit = new ArrayList<>();
                                cancionDAO cn = new cancionDAO();
                                edit = cn.mostrarnombres();
                                for (String cat : edit) {
                            %>
                            <option value="<%=cat%>"><%=cat%>
                            </option>
                            <%
                                }
                            %>
                        </select>
                        <br>
                        <input type="submit" name="btn_mod" value="Editar cancion">
                    </form>
                    <%
                        if (request.getParameter("cbnombre") != null) {
                            String nomb = request.getParameter("cbnombre");
                            cancionDAO dao = new cancionDAO();
                            Cancion cant = dao.mostraruna(nomb);
                            
                    %>

                    <form action="editar.jsp">
                        <input type="hidden" name="idd" value="<%=cant.getId()%>">
                        <p>Nombre</p>
                        <input type="text" name="textNombre" value="<%=cant.getNom()%>">
                        <p>Artista</p>
                        <input type="text" name="textAutor" value="<%=cant.getAutor()%>">
                        <p>Genero</p>
                        <select name="cbgenero">
                            <option value="">Selecciona un genero musical</option>
                            <%

                                Set<String> conj = new TreeSet<>();
                                cancionDAO da = new cancionDAO();
                                conj = da.mostrar();
                                for (String cat : conj) {
                            %>
                            <option value="<%=cat%>"><%=cat%>
                            </option>
                            <%
                                }
                            %>
                        </select>

                        <p>Enlace YT</p>
                        <input type="text" name="textEnlace" value="<%=cant.getEnlace()%>">
                        <p>Comentario</p>
                        <input type="text" name="textComentario" value="<%=cant.getComentario()%>">
                        <%
                        out.print("<input type='submit' name='btn_actualizar' value='Actualizar cancion!'/>");
                        }%>
                        </form>
                    
                    <%
                        if (request.getParameter("textNombre") != null) {
                            boolean agre;
                            int id = Integer.parseInt(request.getParameter("idd"));
                            String nom = request.getParameter("textNombre");
                            String aut = request.getParameter("textAutor");
                            String gen = request.getParameter("cbgenero");
                            String enl = request.getParameter("textEnlace");
                            String com = request.getParameter("textComentario");
                           
                            if (nom != "" && gen != "") {
                                Cancion canedit = new Cancion(nom, aut, gen, enl, com);
                                canedit.setId(id);
                                cancionDAO daos = new cancionDAO();
                                agre = daos.edit(canedit);
                                String mensaje = "<script language='javascript'>alert('La cancion ha sido editada con exito!');</script>";
                                out.println(mensaje);
                            } else {
                                String mensaje = "<script language='javascript'>alert('Ingresa nombre y selecciona un genero de la cancion');</script>";
                                out.println(mensaje);
                            }
                        }
                    %>
                    <br>
                    <br>
                    <br>
                    <hr>
                    <a href="Controlador?accion=home" class="btn btn-success">Volver a ver lista de canciones</a>
                    
                     </div>
            </div>
        </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    </body>
    
</html>
