<%-- 
    Document   : agregar
    Created on : 30/09/2021, 09:31:40 PM
    Author     : Andres Ramirez
--%>

<%@page import="Modelo.Cancion"%>
<%@page import="ModeloDAO.cancionDAO"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.Set"%>
<%@page import="com.google.gson.Gson"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Agregar-laRockola.com</title>
    <div class="contenedor">
        <h1>laRockola.com .<span>&#160;</span></h1>
    </div>
    <hr>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <style>
        .container-fluid{
            text-align: center;
        }
        .col-5 {
            margin:0 auto;
        }
        h1.rockola{
            text-align: center;
            font-weight: bold;
        }
        .border-primary{
            border-radius: 25px;
            background-color: #6f387f;
            border-color: white;
            padding: 20px;
        }
        h2.titu_intro{
            color: white;
        }
        .contenedor {
            margin: auto;
            display: table;
        }
        h1 { 
            position: relative; 
            float: left;
            background: white;
            color: #000;
            font-size: 2.5em;
        }
        h1 span {
            position:absolute;
            right:0;
            width:0;
            background: white;
            border-left: 1px solid #000;
            animation: escribir 5s steps(30) infinite alternate;
        }
        @keyframes escribir {
            from { width: 100% }
            to { width:0 }
        }
    </style>
</head>
<body>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href=Controlador?accion=home">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Agregar</li>
        </ol>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-5">
                <div class="border border-primary">
                    <h2 class="titu_intro">¡Agrega tu cancion favorita!</h2>
                    <br>
                    <form action="agregar.jsp">
                        <div class="input-group input-group-sm mb-3">
                            <span class="input-group-text" id="inputGroup-sizing-sm">Nombre de cancion:</span>
                            <input type="text" class="form-control" name="textNombre" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                        </div>
                        <div class="input-group input-group-sm mb-3">
                            <span class="input-group-text" id="inputGroup-sizing-sm">Artista:</span>
                            <input type="text" name="textAutor" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                        </div>
                        <div class="input-group input-group-sm mb-3">
                            <span class="input-group-text" id="inputGroup-sizing-sm">Genero:</span>
                            <select class="selgen" name="cbgenero">
                                <option value="">Selecciona un genero musical</option>
                                <%

                                    Set<String> conj = new TreeSet<>();
                                    cancionDAO cn = new cancionDAO();
                                    conj = cn.mostrar();
                                    for (String cat : conj) {
                                %>
                                <option value="<%=cat%>"><%=cat%>
                                </option>
                                <%
                                    }
                                %>
                            </select>
                        </div>
                        <div class="input-group input-group-sm mb-3">
                            <span class="input-group-text" id="inputGroup-sizing-sm">Enlace YT:</span>
                            <input type="text" class="form-control" name="textEnlace" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                        </div>
                        <div class="input-group input-group-sm mb-3">
                            <span class="input-group-text" id="inputGroup-sizing-sm">Comentario:</span>
                            <input type="text" class="form-control" name="textComentario" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                        </div>         
                        <br>
                        <br>        
                        <input type="submit" class="btn btn-warning" name="btn_add" value="Agregar cancion">
                    </form>
                    <%
                        if (request.getParameter("textNombre") != null) {
                            boolean agre;
                            String nom = request.getParameter("textNombre");
                            String aut = request.getParameter("textAutor");
                            String gen = request.getParameter("cbgenero");
                            String enl = request.getParameter("textEnlace");
                            String com = request.getParameter("textComentario");
                            Cancion canadd = new Cancion(nom, aut, gen, enl, com);
                            cancionDAO dao = new cancionDAO();
                            agre = dao.add(canadd);
                            if (agre == true) {
                                String mensaje = "<script language='javascript'>alert('Cancion agregada con exito');</script>";
                                out.println(mensaje);
                            }
                        }
                    %>
                    <br>
                </div>
                <br>
                <hr>
                <a href="Controlador?accion=home" class="btn btn-primary">Volver a ver lista de canciones</a>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
</body>
</html>
