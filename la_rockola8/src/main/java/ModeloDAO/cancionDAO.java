
package ModeloDAO;

import Interfaces.CRUD;
import Modelo.Cancion;
import config.bd_conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;


public class cancionDAO implements CRUD {
    
    
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    Statement stmt;
    //cancion c=new cancion();
        

    @Override
    public ArrayList<Cancion> listar() {
        bd_conexion cn=new bd_conexion();
        ArrayList<Cancion>list=new ArrayList<>();
        String sql="select * from cancion";
        rs=cn.consultarBD(sql);
        try {
            
            /*con=cn.getConnection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();*/

            while(rs.next()){
                Cancion can=new Cancion();
                can.setId(rs.getInt("id"));
                can.setNom(rs.getString("nombre"));
                can.setAutor(rs.getString("autor"));
                can.setGen(rs.getString("genero"));
                can.setEnlace(rs.getString("enlace"));
                can.setComentario(rs.getString("comentario"));
                list.add(can);
            } 
        }catch (Exception e){ 
        }
        return list;
    }
    
    public ArrayList<Cancion> listarale(int id) {
        bd_conexion cn=new bd_conexion();
        ArrayList<Cancion>list=new ArrayList<>();
        String sql="select * from cancion where id="+"'"+id+"'";
        rs=cn.consultarBD(sql);
        try {
            while(rs.next()){
                Cancion can=new Cancion();
                can.setId(rs.getInt("id"));
                can.setNom(rs.getString("nombre"));
                can.setAutor(rs.getString("autor"));
                can.setGen(rs.getString("genero"));
                can.setEnlace(rs.getString("enlace"));
                can.setComentario(rs.getString("comentario"));
                list.add(can);
            } 
        }catch (Exception e){ 
        }
        return list;
    }
      
    @Override
    public List list(String gen) {
        bd_conexion cn=new bd_conexion();
        ArrayList<Cancion>list=new ArrayList<>();
        String sql="select * from cancion where genero="+"'"+gen+"'";
        rs=cn.consultarBD(sql);
        try {


            while(rs.next()){
                Cancion can=new Cancion();
                can.setId(rs.getInt("id"));
                can.setNom(rs.getString("nombre"));
                can.setAutor(rs.getString("autor"));
                can.setGen(rs.getString("genero"));
                can.setEnlace(rs.getString("enlace"));
                can.setComentario(rs.getString("comentario"));
                list.add(can);
            } 
        }catch (Exception e){ 
        }
        return list;
    }

    @Override
    public boolean add(Cancion cans) {
       bd_conexion cn= new bd_conexion();
       String sql="INSERT INTO cancion(id,nombre, autor, genero, enlace, comentario) "
                + " VALUES ( '" + 00 + "','" + cans.getNom() + "',"
                + "'" + cans.getAutor() + "','" + cans.getGen() + "','" + cans.getEnlace() + "',"
                + "'" + cans.getComentario() + "');  ";
        try {
            cn.insertarBD(sql);
            return true;
        } catch (Exception e) {
        }
       return false;
    }

    @Override
    public boolean edit(Cancion cant) {
        bd_conexion cn= new bd_conexion();
        
        String sql="UPDATE cancion set nombre='"+cant.getNom()+"',"+"autor='"+cant.getAutor()+"',"+"genero='"+cant.getGen()+"',"+"enlace='"+cant.getEnlace()
                +"',comentario='"+cant.getComentario()+"' where id="+cant.getId();
 
        try {
            cn.insertarBD(sql);
            return true;
        } catch (Exception e) {
        }
       return false;
    }
    
    public Cancion mostraruna(String nom) {
        bd_conexion cn = new bd_conexion();
        String sql = "select * from cancion where nombre='"+nom+"'";
        rs = cn.consultarBD(sql);
        Cancion can=new Cancion();
        try {
            while (rs.next()) {
                can.setId(rs.getInt("id"));
                can.setNom(rs.getString("nombre"));
                can.setAutor(rs.getString("autor"));
                can.setGen(rs.getString("genero"));
                can.setEnlace(rs.getString("enlace"));
                can.setComentario(rs.getString("comentario"));
            }
        } catch (Exception e) {
        }
        return can;
    }

    @Override
    public boolean eliminar(String nom) {
        bd_conexion cn= new bd_conexion();
        String sql="delete from cancion where nombre='"+nom+"'";
        try {
            cn.insertarBD(sql);
            return true;
        } catch (Exception e) {
        }
       return false;
    }
    
    public List<String> mostrarnombres() {
        bd_conexion cn = new bd_conexion();
        List<String> list = new ArrayList<>();
        String sql = "select * from cancion";
        rs = cn.consultarBD(sql);
        try {

            while (rs.next()) {
                list.add(rs.getString("nombre"));
            }
        } catch (Exception e) {
        }
        return list;
    }
        
    public Set<String> mostrar(){
        bd_conexion con = new bd_conexion();
        String com = "SELECT genero FROM cancion;";
        Set<String> generos =new TreeSet<>();
        //Map<Integer,String> mapa = new HashMap<Integer,String>(); 
        ResultSet rs = con.consultarBD(com);
        try {
            while(rs.next()){
                //mapa.put(rs.getInt("id"),rs.getString("genero"));
                System.out.println(rs.getString("genero"));
                generos.add(rs.getString("genero"));
            }return generos;
            
        } catch (SQLException ex) {
            Logger.getLogger(cancionDAO.class.getName()).log(Level.SEVERE, null, ex);
            return generos;
        }
    }
    
    public List<Integer> selectid(){
        bd_conexion con = new bd_conexion();
        String com = "SELECT id FROM cancion;";
        List<Integer> ids =new ArrayList<>();
        ResultSet rs = con.consultarBD(com);
        try {
            while(rs.next()){
                System.out.println(rs.getString("id"));
                ids.add(rs.getInt("id"));
            }return ids;
            
        } catch (SQLException ex) {
            Logger.getLogger(cancionDAO.class.getName()).log(Level.SEVERE, null, ex);
            return ids;
        }
    }

    
    public static void main(String[] args) {
        cancionDAO can = new cancionDAO();
        can.mostrar();
    }
  
}