package Modelo;

public class Cancion {

    private int id;
    private String nom;
    private String autor;
    private String gen;
    private String enlace;
    private String comentario;

    public Cancion() {

    }

    public Cancion(String nom, String autor, String gen, String enlace, String comentario) {
        this.nom = nom;
        this.autor = autor;
        this.gen = gen;
        this.enlace = enlace;
        this.comentario = comentario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getGen() {
        return gen;
    }

    public void setGen(String gen) {
        this.gen = gen;
    }

    public String getEnlace() {
        return enlace;
    }

    public void setEnlace(String enlace) {
        this.enlace = enlace;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }
}