
package config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class bd_conexion {
    Connection con;
    private PreparedStatement ps=null;
    private Statement stmt=null;
    private ResultSet rs=null;
    String sql ="";
        
    public bd_conexion(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/s37g8larockola_cancion","root","root");
            con.setTransactionIsolation(8);
        } catch (Exception e) {
            System.err.println("Error:" +e);
        }
    }
    public Connection getConnection(){
        //pstm=con.prepareStatement(sql)
        return con;
    }
    
    public ResultSet consultarBD(String sentencia) {
        try {
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            rs = stmt.executeQuery(sentencia);
        } catch (SQLException sqlex) {
        } catch (RuntimeException rex) {
        } catch (Exception ex) {
        }
        return rs;
    }
    public boolean insertarBD(String sentencia) {
        try {
            stmt = con.createStatement();
            stmt.execute(sentencia);
        } catch (SQLException sqlex) {
            return false;
        } catch (RuntimeException rex) {
        } catch (Exception ex) {
        }
        return true;
    }
    
  
}