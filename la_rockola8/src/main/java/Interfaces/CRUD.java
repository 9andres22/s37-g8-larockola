
package Interfaces;

import Modelo.Cancion;
import java.util.List;


public interface CRUD {
    public List listar();
    public List list(String gen);
    public boolean add(Cancion cans);
    public boolean edit(Cancion cant);
    public boolean eliminar(String nom);
}
