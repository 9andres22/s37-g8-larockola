-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: s37g8rockola
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cancion`
--

DROP TABLE IF EXISTS `cancion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cancion` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `autor` varchar(45) DEFAULT NULL,
  `genero` varchar(45) DEFAULT NULL,
  `enlace` text NOT NULL,
  `comentario` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cancion`
--

LOCK TABLES `cancion` WRITE;
/*!40000 ALTER TABLE `cancion` DISABLE KEYS */;
INSERT INTO `cancion` VALUES (1,'Forever','Kiss','Rock','https://www.youtube.com/watch?v=hLQl3WQQoQ0','conocida en todo lado'),(2,'Hello','Adele','POP','https://www.youtube.com/watch?v=YQHsXMglC9A','famosa'),(17,'Bella','Wolfine','Reggaeton','https://www.youtube.com/watch?v=jjuSfhYiup8',''),(18,'Ay!','Rikarena','Merengue','https://www.youtube.com/watch?v=tRpRavSeAac',''),(19,'Without You','Avicii','Electro House','https://www.youtube.com/watch?v=WRz2MxhAdJo',''),(20,'Propuesta Indecente','Romeo Santos','Bachata','https://www.youtube.com/watch?v=QFs3PIZb3js',''),(21,'Tal para cual','Joe Arroyo','Salsa','https://www.youtube.com/watch?v=oReuYMzIokg','Pasos prohibidos'),(22,'Shape of you','Ed Sheeran','POP','https://www.youtube.com/watch?v=JGwWNGJdvx8',''),(23,'Morena','ChocQuibTown ft Lil silvio & El vega','POP','https://www.youtube.com/watch?v=Ehh5wcRrh8s',''),(24,'The real Slim Shady','Eminem','Rap','https://www.youtube.com/watch?v=eJO5HU_7_1w',''),(25,'Three Little brids','Bob Marley & The Wailers','Reggae','https://www.youtube.com/watch?v=HNBCVM4KbUM','Muy famosa'),(26,'Bailar Contigo','Monsieur Periné','POP','https://www.youtube.com/watch?v=mT7_qIFBa3Y',''),(27,'Laugh Now Cry Later','Drake','Rap','https://www.youtube.com/watch?v=JFm7YDVlqnI&list=RDQMKuqK6mE0KNA&start_radio=1',''),(29,'Hotel California','Eagles','Rock','https://www.youtube.com/watch?v=EqPtz5qN7HM',''),(30,'Otherside','Red Hot Chili Peppers','Rock','https://www.youtube.com/watch?v=_m7_xZN8D7M',''),(33,'Pegame Tu Vicio','Eddy Herrera','Merengue','https://www.youtube.com/watch?v=XfdYax73Ehk','Para azotar baldosa'),(34,'Flaca','Andrés Calamaro','Rock','https://www.youtube.com/watch?v=UCF9oHXhDMU',''),(35,'The Nights','Avicii','Electro House','https://www.youtube.com/watch?v=UtF6Jej8yb4','');
/*!40000 ALTER TABLE `cancion` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-05 20:26:39
